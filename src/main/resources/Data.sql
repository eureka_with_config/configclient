insert into product
  (id,
   product_name,
   product_category,
   product_code,
   product_title,
   product_desc,
   price)
values
  (101,
   'Product 1',
   'Mobile',
   'Code_1',
   'Product Number 1',
   'Product 1 Mobile',
   501);
   
insert into product
  (id,
   product_name,
   product_category,
   product_code,
   product_title,
   product_desc,
   price)
values
  (102,
   'Product 2',
   'Mobile',
   'Code_2',
   'Product Number 2',
   'Product 2 Mobile',
   502);
   
insert into product
  (id,
   product_name,
   product_category,
   product_code,
   product_title,
   product_desc,
   price)
values
  (103,
   'Product 3',
   'Mobile',
   'Code_3',
   'Product Number 3',
   'Product 3 Mobile',
   503);
   
insert into product
  (id,
   product_name,
   product_category,
   product_code,
   product_title,
   product_desc,
   price)
values
  (104,
   'Product 4',
   'Tablet',
   'Code_4',
   'Product Number 4',
   'Product 4 Mobile',
   504);
   
insert into product
  (id,
   product_name,
   product_category,
   product_code,
   product_title,
   product_desc,
   price)
values
  (105,
   'Product 5',
   'Tablet',
   'Code_5',
   'Product Number 5',
   'Product 5 Mobile',
   505);