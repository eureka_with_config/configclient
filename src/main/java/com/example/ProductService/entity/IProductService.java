package com.example.ProductService.entity;

public interface IProductService {

	public void getAllProducts();
	public void getProduct();
	public void insertProduct();
}
