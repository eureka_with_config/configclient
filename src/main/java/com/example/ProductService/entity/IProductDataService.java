package com.example.ProductService.entity;

import java.util.List;

public interface IProductDataService {

	public List<IProduct> getAllProducts();
	public IProduct getProductDetails(Integer productId);
	public List<IProduct> getDummyProducts();
}
