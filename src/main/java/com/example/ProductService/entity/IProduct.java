package com.example.ProductService.entity;

public interface IProduct {
	
	public Long getProductId();

	public void setProductId(Long productId);

	public String getProductName();

	public void setProductName(String productName);

	public String getProductCategory();

	public void setProductCategory(String productCategory);

	public String getProductCode();

	public void setProductCode(String productCode);

	public String getProductTitle();

	public void setProductTitle(String productTitle);

	public String getProductDescription();

	public void setProductDescription(String productDescription);

	public Double getPrice();

	public void setPrice(Double price);
}
