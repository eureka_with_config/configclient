package com.example.ProductService.Process;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.ProductService.DAO.IProductDaoJPQL;
import com.example.ProductService.entity.IProduct;
import com.example.ProductService.entity.IProductDataService;
import com.example.ProductService.entity.impl.Product;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Component
public class ProductDataService implements IProductDataService {
	
	@Autowired
	IProductDaoJPQL productDaoJPQL;

	@HystrixCommand(fallbackMethod = "getDummyProducts")
	public List<IProduct> getAllProducts() {
		return productDaoJPQL.getProducts();
	}
	
	public IProduct getProductDetails(Integer productId) {
		return productDaoJPQL.getProductById(productId);
	}
	
	public List<IProduct> getDummyProducts() {
		ArrayList<IProduct> productList = new ArrayList<>();
		IProduct product = new Product();
		productList.add(product);
		return productList;
	}
}
