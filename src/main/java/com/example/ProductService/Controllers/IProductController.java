package com.example.ProductService.Controllers;

import java.util.List;
import org.springframework.web.bind.annotation.PathVariable;
import com.example.ProductService.entity.IProduct;

public interface IProductController {
	
	public List<IProduct> getAllProducts();
	public IProduct getProduct(@PathVariable int productId);

}
