package com.example.ProductService.Controllers.Impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.ProductService.entity.IProduct;
import com.example.ProductService.entity.IProductDataService;

@RestController
public class ProductController {
	
	@Autowired
	IProductDataService productDataService;
	
	@Autowired
	DataSource dataSource;
	
	@GetMapping("/")
	public String getTest() {
		return "Hi";
	}

	@GetMapping("/products")
	public List<IProduct> getAllProducts() {
		return productDataService.getAllProducts();
	}
	
	@GetMapping("/products/{productId}")
	public IProduct getProduct(@PathVariable int productId) {
		return productDataService.getProductDetails(productId);
	}
	
	@Value("${datasource-driver-class-name}")
	private String valorFijo;
	
	@GetMapping("/dummy")
	public String getDummyData() {
		return this.valorFijo;
	}
	
	@GetMapping("/datasource")
	public String getPortData() {
		return this.dataSource.toString();
	}
}
