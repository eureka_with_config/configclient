package com.example.ProductService.DAO.impl;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.ProductService.DAO.IProductDaoJPQL;
import com.example.ProductService.DAO.IProductRepository;
import com.example.ProductService.entity.IProduct;
import com.example.ProductService.entity.impl.Product;

@Repository
public class ProductDaoJPQL implements IProductDaoJPQL{
	
	@Autowired
	private IProductRepository dataRepository;

	@Override
	public ArrayList<IProduct> getProducts() {
		ArrayList<IProduct> productList = new ArrayList<>();
		dataRepository.findAll().forEach(p -> productList.add(p));
		return productList;
	}

	@Override
	public IProduct getProductById(Integer productId) {
		Optional<Product> output =  dataRepository.findById(productId);
		if(output.isPresent()) {
			return output.get();
		}
		return null;
	}

	@Override
	public void addProduct(IProduct product) {
		dataRepository.save((Product)product);
	}

	@Override
	public void deleteProduct(Integer productId) {
		dataRepository.deleteById(productId);
	}

}
