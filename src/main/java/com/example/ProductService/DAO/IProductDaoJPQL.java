package com.example.ProductService.DAO;

import java.util.ArrayList;

import com.example.ProductService.entity.IProduct;

public interface IProductDaoJPQL {

	public ArrayList<IProduct> getProducts();
	public IProduct getProductById(Integer productId);
	public void addProduct(IProduct product);
	public void deleteProduct(Integer productId);
}
