package com.example.ProductService.DAO;

import org.springframework.data.repository.CrudRepository;

import com.example.ProductService.entity.impl.Product;

public interface IProductRepository extends CrudRepository<Product, Integer>{

}
